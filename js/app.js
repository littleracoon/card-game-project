//document.getElementById('add').addEventListener('click', loadCards);
const jsonLink = '../cards.json',
wrapper = document.querySelector('.playDesk');

let cardOrder;

// Get local json data
function loadCards(url) {
  fetch(url)
    .then(function(res){
      return res.json();
    })
    .then(function Add(data) {

      function compareRandom(a, b) {
        return Math.floor(Math.random() * 51);
      }
      cards = data.sort(compareRandom);
      cardOrder = cards;

      cards.forEach(function(card,i) {

        if(i<7){
          const div = document.createElement('div');
          div.setAttribute('class','card');
          
          div.innerHTML = `
              <h2>${card.symbol}</h2>
              <div class="cardSuit cardSuit-${card.suit}"></div>
              <a href="#" class="card__body" id="${card.number}-${card.suit}"></a>
          ` ;

          document.getElementById(`place-${i+1}`).append(div);
        }

      });

      //return cards;
      
    })
    .catch(function(err){
      //console.log(err);
    });
};

 function renderCards(){

 }



loadCards(jsonLink); 



document.getElementById('add').addEventListener('click', function(){
  console.log(cardOrder);
});

// function CardUp(e) {
//   const elem = e;
//   const link = e.target;
//   link.parentElement.style.position = 'absolute';
//   link.parentElement.style.left = elem.pageX - 25 + "px";
//   link.parentElement.style.top = elem.pageY - 25 + "px";   
// };

// function moveCards(e) {
//   const elem = e;
//   const link = e.target;
//   console.log(link);
//   elem.preventDefault();
//   if(link.getAttribute('class') === 'card__body'){ 

//     document.addEventListener('mousemove', CardUp);

//     console.log(link.type);
  
//   }
// };

// function stopMoveCards(e) {
//   const elem = e;
//   const link = e.target;
//   console.log(link);
//   elem.preventDefault();
//   if(link.getAttribute('class') === 'card__body'){

//     document.removeEventListener('mousemove',  CardUp);
      
//     console.log(link.type);

//   }
// };


window.addEventListener('load', function () {

  const cardItems = document.getElementsByClassName('card__body');

  for (let i = 0; i < cardItems.length; i++) {

    const self = cardItems[i];

    function CardUp(e) {
      e.preventDefault();
      self.parentElement.style.position = 'absolute';
      self.parentElement.style.left = e.pageX - 25 + "px";
      self.parentElement.style.top = e.pageY - 25 + "px";   
    };

    function moveCards(e) {
      e.preventDefault();
      console.log(e.type);
     
      document.addEventListener('mousemove', CardUp);
      //console.log(self);
    };

    function stopMoveCards(e) {
      e.preventDefault();
      console.log(e.type);

      document.removeEventListener('mousemove', CardUp);  
      //console.log(self);
    
    };

    //console.log(cardItems[i]);
    cardItems[i].addEventListener('mousedown', moveCards);
    cardItems[i].addEventListener('mouseup', stopMoveCards);

  };

});